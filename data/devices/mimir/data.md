---
name: "Volla Tablet"
deviceType: "tablet"
image: "https://gitlab.com/design3680819/ut-hw-models/-/raw/main/Tablets/Volla%20Tablet/VollaTablet.png"
buyLink: "https://volla.online/de/shop/volla-tablet/"
description: "The Volla tablet combines maximum comfort, performance and data protection. With its 12.3 inch display, powerful MTK G99 processor and up to two SIM cards, it offers the 3-in-1 experience of a laptop, tablet and smartphone. Perfect for productivity, creativity, internet and gaming. You can get the device pre-installed with Ubuntu Touch or (in the future) as a 2nd operating system with the unique multi-boot feature of Volla OS."
#subforum: "???/volla-tablet" # FIXME: missing from https://forums.ubports.com/category/89/volla
price:
  avg: 698
  currency: "EUR"
  currencySymbol: "€"

deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM (2x Cortex-A76 @ 2.2 GHz + 6x Cortex-A55 @ 2.0 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio G99, MT8781 (tablet version of MT6789)"
  - id: "gpu"
    value: "ARM Mali-G57 MC2 (Valhall) @ 850 MHz, 2 cores"
  - id: "rom"
    value: "512 GB, UFS 2.2"
  - id: "ram"
    value: "12 GB, LPDDR4X @ 2133 MHz"
  - id: "android"
    value: "14.0"
  - id: "battery"
    value: "10 000 mAh, Li-Polymer"
  - id: "display"
    value: '12.6" IPS, 2560 x 1600 (240 PPI), Rounded corners'
  - id: "rearCamera"
    value: "13MP, LED flash"
  - id: "frontCamera"
    value: "5MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "278.5 mm x 174 mm x 7.5 mm"
  - id: "weight"
    value: "500 g"
  - id: "releaseDate"
    value: "December 2024"

contributors:
  - name: "Hallo Welt Systeme UG"
    role: "Phone maker"
    forum: "https://volla.online/en/"
    photo: "https://avatars.githubusercontent.com/u/62447194" # https://github.com/HelloVolla
  - name: "TheKit"
    role: "Developer"
    forum: "https://forums.ubports.com/user/thekit"
  - name: "Deathmist"
    role: "Developer"
    forum: "https://forums.ubports.com/user/deathmist"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-3171/3171-profileavatar-1613145487767.png"

communityHelp:
  - name: "Telegram - @utonvolla"
    link: "https://t.me/utonvolla"

sources:
  portType: "reference"
  portPath: "halium13"
  deviceGroup: "volla-tablet"
  deviceSource: "volla-mimir"
  kernelSource: "android_kernel_volla_mt8781"

externalLinks:
  - name: "UBports Installer config"
    link: "https://github.com/ubports/installer-configs/blob/master/v2/devices/mimir.yml"
  - name: "Source for details on this page"
    link: "https://gitlab.com/ubports/infrastructure/devices.ubuntu-touch.io/-/tree/main/data/devices/mimir"

seo:
  description: "Get your Volla Tablet with latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, VollaTablet, Volla Tablet, Privacy Tablet"
---
