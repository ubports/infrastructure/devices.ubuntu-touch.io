---
name: "rabbit r1"
deviceType: "other"
image: "https://camo.githubusercontent.com/f2a16fcb0679be842c06ea87bb930df0b26f0d235296243e9cc6af2b3a8326ce/68747470733a2f2f706c6174666f726d2e74686576657267652e636f6d2f77702d636f6e74656e742f75706c6f6164732f73697465732f322f63686f7275732f75706c6f6164732f63686f7275735f61737365742f66696c652f32353231323738322f7261626269745f72315f66726f6e742e6a7067"
buyLink: "https://rabbit.tech/"
description: "The Rabbit R1 is an AI-powered personal assistant device developed by Rabbit Inc., designed to simplify daily tasks through voice commands and touch interactions."
subforum: ""
price:
  avg: 199
  currency: "USD"
  currencySymbol: "$"

deviceInfo:
  - id: "cpu"
    value: "Octa-core 2.3 GHz Cortex-A53"
  - id: "chipset"
    value: "MediaTek Helio P35 (12nm)"
  - id: "gpu"
    value: "PowerVR GE8320"
  - id: "rom"
    value: "128 GB"
  - id: "ram"
    value: "4 GB"
  - id: "android"
    value: "13.0, rabbitOS"
  - id: "battery"
    value: "1000 mAh, Li-Polymer, Non-Removable"
  - id: "display"
    value: "2.88-inch TFT touchscreen, 60Hz"
  - id: "rearCamera"
    value: "8 MP, 3264x2448 resolution, 1080p video at 24 fps"
  - id: "frontCamera"
    value: "Rear Camera can be used as front camera."
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "78 mm x 78 mm x 13 mm (3 in x 3 in x 0.5 in)"
  - id: "weight"
    value: "115 g"
  - id: "releaseDate"
    value: "Jan 2024"

contributors:
  - name: "Aryan Sinha (techyminati)"
    role: "Maintainer"
    forum: "https://github.com/techyminati"
    photo: "https://avatars.githubusercontent.com/u/63485082"

sources:
  portType: "community"
  portPath: "android12"
  deviceGroup: "rabbit-r1"
  deviceSource: "rabbit-r1"
  kernelSource: "kernel-rabbit-mt6765"

externalLinks:
  - name: "UBports Installer config"
    link: "https://github.com/ubports/installer-configs/blob/master/v2/devices/r1.yml"
  - name: "Source for details on this page"
    link: "https://gitlab.com/ubports/infrastructure/devices.ubuntu-touch.io/-/tree/main/data/devices/r1"

seo:
  description: "Get your rabbit r1 with latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, rabbit r1"
---
