---
name: "Volla Phone Quintus"
deviceType: "phone"
image: "https://gitlab.com/design3680819/ut-hw-models/-/raw/main/Phones/Volla%20Quintus/Volla%20Quintus.png"
buyLink: "https://volla.online/de/shop/volla-phone-quintus/"
description: "With its XL display, triple camera, 5G capability, Dimensity 7050 processor and 8/256 GB memory, the Volla Phone Quintus is a real flagship model running an uncompromising privacy respecting, open source operating system, combined with a unique user experience. You can get the device pre-installed with Ubuntu Touch or as a 2nd operating system with the unique multi-boot feature of Volla OS."
#subforum: "???/volla-phone-quintus" # FIXME: missing from https://forums.ubports.com/category/89/volla
price:
  avg: 719
  currency: "EUR"
  currencySymbol: "€"

deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM (2x Cortex-A78 @ 2.6 GHz + 6x Cortex-A55 @ 2.0 GHz cores)"
  - id: "chipset"
    value: "MediaTek Dimensity 7050, MT6877"
  - id: "gpu"
    value: "ARM Mali-G68 MP4 (Valhall) @ 800 MHz, 4 cores"
  - id: "rom"
    value: "256 GB, UFS"
  - id: "ram"
    value: "8 GB, LPDDR5"
  - id: "android"
    value: "14.0"
  - id: "battery"
    value: "4700 mAh, Li-Polymer, 66W fast charging"
  - id: "display"
    value: '6.78" AMOLED, 2400 x 1080 (388 PPI), up to 120 Hz, Hole-punch camera, Under-display fingerprint sensor, Curved sides, Rounded corners'
  - id: "rearCamera"
    value: "50MP (f/1.9, 2160p30 video) + 8MP (f/2.4, ultra wide-angle) + 2MP (macro) lenses, PDAF, dual-tone flash"
  - id: "frontCamera"
    value: "16MP (f/2.5)"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "164.2 mm x 74.7 mm x 8.8 mm"
  - id: "weight"
    value: "205 g"
  - id: "releaseDate"
    value: "October 2024"

contributors:
  - name: "Hallo Welt Systeme UG"
    role: "Phone maker"
    forum: "https://volla.online/en/"
    photo: "https://avatars.githubusercontent.com/u/62447194" # https://github.com/HelloVolla
  - name: "TheKit"
    role: "Developer"
    forum: "https://forums.ubports.com/user/thekit"
  - name: "Deathmist"
    role: "Developer"
    forum: "https://forums.ubports.com/user/deathmist"
    photo: "https://forums.ubports.com/assets/uploads/profile/uid-3171/3171-profileavatar-1613145487767.png"

communityHelp:
  - name: "Telegram - @utonvolla"
    link: "https://t.me/utonvolla"

sources:
  portType: "reference"
  portPath: "halium13"
  deviceGroup: "volla-phone-quintus"
  deviceSource: "volla-algiz"
  kernelSource: "kernel-volla-mt6877"

externalLinks:
  - name: "UBports Installer config"
    link: "https://github.com/ubports/installer-configs/blob/master/v2/devices/algiz.yml"
  - name: "Source for details on this page"
    link: "https://gitlab.com/ubports/infrastructure/devices.ubuntu-touch.io/-/tree/main/data/devices/algiz"

seo:
  description: "Get your Volla Phone Quintus with latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, VollaPhoneQuintus, Volla Phone Quintus, Privacy Phone"
---
