---
variantOf: "gram"
name: "Xiaomi Redmi Note 9 Pro (Global)"
description: "The Redmi Note 9 Pro (Global) is a good level Android smartphone, nice for photos, that can satisfy even the most demanding of users."

deviceInfo:
  - id: "ram"
    value: "6GB/8GB"
  - id: "display"
    value: "IPS LCD, 6.67 inches, 107.4 cm2 (~84.5% screen-to-body ratio) 1080 x 2400 pixels, 20:9 ratio (~395 ppi density)"
  - id: "rearCamera"
    value: "64MP(wide), 8MP(ultrawide), 5MP(macro), 2MP(depth)"
  - id: "releaseDate"
    value: "May 2020"
---
